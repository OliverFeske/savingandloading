﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.Text;
using System.IO;
using UnityEngine;

[System.Serializable]
public class XMLManager<T>
{
	public static List<T> LoadData(string pathToFile, Database<T> dataBase)
	{
		if (File.Exists(pathToFile))
		{
			using(StreamReader reader = new StreamReader(pathToFile))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(Database<T>));
				dataBase = serializer.Deserialize(reader) as Database<T>;
				return dataBase.generalDataList;
			}
		}
		else { return null; }
	}

	public static void SaveData(List<T> dataList, string pathToFile, Database<T> dataBase)
	{
		Debug.Log(dataList.Count);
		dataBase.generalDataList = dataList;
		Encoding encoding = Encoding.GetEncoding("UTF-8");

		using (StreamWriter writer = new StreamWriter(pathToFile, false, encoding))
		{
			XmlSerializer serializer = new XmlSerializer(typeof(Database<T>));
			serializer.Serialize(writer, dataBase);
		}
	}
}
