﻿using System.Collections.Generic;
using System.Xml.Serialization;

[System.Serializable]
public class Database<T>
{
	[XmlArray("GeneralData")] public List<T> generalDataList = new List<T>();
}
