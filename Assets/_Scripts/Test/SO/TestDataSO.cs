﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Data/TestData")]
public class TestDataSO : ScriptableObject
{
	public string Name = "Ruediger";
	public float Heigth = 1.85f;
	public int Age = 95;
	public Vector3 StartPosition = Vector3.one;
	public GameObject otherObject;
}
