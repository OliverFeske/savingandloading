﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
	public Color Color;
	public string Name;
	public int ATKDamage;
	public int Durability;
	public int CurrentDurability;

	void Start()
	{
		transform.GetChild(0).GetComponent<Renderer>().material.color = Color;
	}
}
