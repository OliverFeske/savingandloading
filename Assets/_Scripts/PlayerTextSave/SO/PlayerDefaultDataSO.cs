﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "Data/PlayerData")]
public class PlayerDefaultDataSO : ScriptableObject
{
	public Weapon EquippedWeapon;
	public Vector3 SpawnPos;
	public string PlayerName;
	public int CurrentHealth;
	public int Level;
	public int Score;
}
