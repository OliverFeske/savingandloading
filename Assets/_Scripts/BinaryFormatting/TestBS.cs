﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBS : MonoBehaviour
{
	private BinarySerializer serializer;
	[SerializeField] private GameSaveData gameData;
	[SerializeField] private string gameDataPath;
	[SerializeField] private string fileName;

	void Awake()
	{
		serializer = GetComponent<BinarySerializer>();
		gameData = new GameSaveData(3, "Steve", transform.position);

	}

	void Update()
	{

	}

	public void Save()
	{
		serializer.SaveGameData(Application.streamingAssetsPath + "/" + gameDataPath + "/", fileName, gameData);
	}

	public void Load()
	{
		gameData = serializer.LoadGameData(Application.streamingAssetsPath + "/" + gameDataPath + "/", fileName);
		Vector3 pos = gameData.PlayerPosition.ToVector3();
		transform.position = pos;
		Debug.Log(gameData.CurrentLevel.ToString());
	}
}
