﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour, ITransformable1
{
	private Renderer rend;

	void Awake()
	{
		rend = GetComponent<Renderer>();
	}

	public void Move(Vector3 dir)
	{
		transform.position += 0.5f * dir;
	}

	public void Scale(float factor)
	{
		transform.localScale *= factor;
		rend.material.color = Random.ColorHSV();
	}
}
