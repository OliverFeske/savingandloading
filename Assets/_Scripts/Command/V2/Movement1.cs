﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class Movement1 : MonoBehaviour
//{
//	private ICommand1 moveLeft;
//	private ICommand1 moveRight;
//	private ICommand1 moveUp;
//	private ICommand1 moveDown;
//	private Stack<ICommand1> undoHistory = new Stack<ICommand1>();
//	private Stack<ICommand1> redoHistory = new Stack<ICommand1>();

//	void Start()
//	{
//		moveLeft = new MoveCommand1(Vector3.left, transform);
//		moveRight = new MoveCommand1(Vector3.right, transform);
//		moveUp = new MoveCommand1(Vector3.up, transform);
//		moveDown = new MoveCommand1(Vector3.down, transform);
//	}

//	void Update()
//	{
//		if (Input.GetKeyDown(KeyCode.A))
//		{
//			moveLeft.Execute();
//			undoHistory.Push(moveLeft);
//			redoHistory.Clear();
//		}

//		if (Input.GetKeyDown(KeyCode.D))
//		{
//			moveRight.Execute();
//			undoHistory.Push(moveRight);
//			redoHistory.Clear();
//		}

//		if (Input.GetKeyDown(KeyCode.W))
//		{
//			moveUp.Execute();
//			undoHistory.Push(moveUp);
//			redoHistory.Clear();
//		}

//		if (Input.GetKeyDown(KeyCode.S))
//		{
//			moveDown.Execute();
//			undoHistory.Push(moveDown);
//			redoHistory.Clear();
//		}

//		if (Input.GetKeyDown(KeyCode.Z))
//		{
//			ICommand lastCmd = undoHistory.Pop();
//			lastCmd.Undo();
//			redoHistory.Push(lastCmd);
//		}

//		if (Input.GetKeyDown(KeyCode.R))
//		{
//			redoHistory.Pop().Execute();
//		}
//	}
//}
