﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCP : MonoBehaviour
{
	private Stack<ICommand1> undoStack = new Stack<ICommand1>();
	private Stack<ICommand1> redoStack = new Stack<ICommand1>();
	[SerializeField] private Cube startCube;
	[SerializeField] private GameObject cubePre;
	private ITransformable1 curTransformable;
	private Camera cam;

	void Awake()
	{
		cam = Camera.main;

		// only works with exeption, not with a debug
		curTransformable = startCube == null ? throw new System.Exception("Cube is null") : startCube.GetComponent<ITransformable1>();

		//if (curMoveable == null)
		//	Debug.LogWarning("cube is null!");
		//else
		//	curMoveable = startCube.GetComponent<IMoveable1>();
	}

	void Update()
	{
		if (Input.GetMouseButtonDown(0) &&
			Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
		{
			var transformable = hit.collider.GetComponent<ITransformable1>();

			if (transformable != null)
				curTransformable = transformable;
		}
		GetInput();
	}

	private void GetInput()
	{
		if (Input.GetKeyDown(KeyCode.W))
		{
			redoStack.Clear();
			undoStack.Push(new MoveCommand1(Vector3.up, curTransformable));
			undoStack.Peek().Execute();
		}
		if (Input.GetKeyDown(KeyCode.A))
		{
			redoStack.Clear();
			undoStack.Push(new MoveCommand1(Vector3.left, curTransformable));
			undoStack.Peek().Execute();
		}
		if (Input.GetKeyDown(KeyCode.S))
		{
			redoStack.Clear();
			undoStack.Push(new MoveCommand1(Vector3.down, curTransformable));
			undoStack.Peek().Execute();
		}
		if (Input.GetKeyDown(KeyCode.D))
		{
			redoStack.Clear();
			undoStack.Push(new MoveCommand1(Vector3.right, curTransformable));
			undoStack.Peek().Execute();
		}
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			redoStack.Clear();
			undoStack.Push(new ScaleCommand1(2f, curTransformable));
			undoStack.Peek().Execute();
		}
		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			redoStack.Clear();
			undoStack.Push(new ScaleCommand1(0.5f, curTransformable));
			undoStack.Peek().Execute();
		}
		if (Input.GetKeyDown(KeyCode.U))
		{
			if (undoStack.Count < 1) { return; }
			redoStack.Push(undoStack.Pop());
			redoStack.Peek().Undo();
		}
		if (Input.GetKeyDown(KeyCode.R))
		{
			if (redoStack.Count < 1) { return; }
			undoStack.Push(redoStack.Pop());
			undoStack.Peek().Execute();
		}
		if (Input.GetKeyDown(KeyCode.Space))
		{
			var newCube = new CubeCreator(
				new Vector3(10f, 10f, 5f), 
				new Quaternion(90f, 17f, 23f, 1f), 
				new Vector3(1.5f, 2f, 0.3f), 
				cubePre
				);
			newCube.Create();
		}
	}
}
