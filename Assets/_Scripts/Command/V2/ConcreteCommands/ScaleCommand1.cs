﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleCommand1 : ICommand1
{
	private IScalable1 scalable;
	private float factor;

	public ScaleCommand1(float _factor, IScalable1 _scalable)
	{
		factor = _factor;
		scalable = _scalable;
	}

	public void Execute()
	{
		scalable.Scale(factor);
	}

	public void Undo()
	{
		scalable.Scale(1 / factor);
	}
}
