﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCommand1 : ICommand1
{
	private Vector3 dir;
	private IMoveable1 moveable;

	public MoveCommand1(Vector3 _dir, IMoveable1 _moveable)
	{
		dir = _dir;
		moveable = _moveable;
	}

	public void Execute()
	{
		moveable.Move(dir);
	}

	public void Undo()
	{
		moveable.Move(-dir);
	}
}
