﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour, ITransformable1, ISpawnableGameObject
{
	public void Move(Vector3 dir)
	{
		transform.position += dir;
	}

	public void Scale(float factor)
	{
		transform.localScale *= factor;	
	}

	public void SetValues(Vector3 _pos, Quaternion _rotation, Vector3 _scale)
	{
		transform.position = _pos;
		transform.rotation = _rotation;
		transform.localScale = _scale;
	}
}