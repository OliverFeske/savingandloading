﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
	private ICommand moveLeft;
	private ICommand moveRight;
	private ICommand moveUp;
	private ICommand moveDown;
	private Stack<ICommand> undoHistory = new Stack<ICommand>();
	private Stack<ICommand> redoHistory = new Stack<ICommand>();

	void Start()
	{
		moveLeft = new MoveCommand(Vector3.left, transform);
		moveRight = new MoveCommand(Vector3.right, transform);
		moveUp = new MoveCommand(Vector3.up, transform);
		moveDown = new MoveCommand(Vector3.down, transform);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.A))
		{
			moveLeft.Execute();
			undoHistory.Push(moveLeft);
			redoHistory.Clear();
		}

		if (Input.GetKeyDown(KeyCode.D))
		{
			moveRight.Execute();
			undoHistory.Push(moveRight);
			redoHistory.Clear();
		}

		if (Input.GetKeyDown(KeyCode.W))
		{
			moveUp.Execute();
			undoHistory.Push(moveUp);
			redoHistory.Clear();
		}

		if (Input.GetKeyDown(KeyCode.S))
		{
			moveDown.Execute();
			undoHistory.Push(moveDown);
			redoHistory.Clear();
		}

		if (Input.GetKeyDown(KeyCode.Z))
		{
			ICommand lastCmd = undoHistory.Pop();
			lastCmd.Undo();
			redoHistory.Push(lastCmd);
		}

		if (Input.GetKeyDown(KeyCode.R))
		{
			redoHistory.Pop().Execute();
		}
	}
}
