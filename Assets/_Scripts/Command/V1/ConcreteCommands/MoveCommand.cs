﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCommand : ICommand
{
	private Vector3 dir;
	private Transform transform;

	public MoveCommand(Vector3 _dir, Transform _transform)
	{
		dir = _dir;
		transform = _transform;
	}

	public void Execute()
	{
		transform.position += dir;
	}

	public void Undo()
	{
		transform.position -= dir;
	}
}
